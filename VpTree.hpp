#ifndef _SMTL_VPTREE_H
#define _SMTL_VPTREE_H

#include "random.cpp"
#include <algorithm>
#include <iterator>
#include <iomanip>
#include <stack>
#include "VPSelectors.hpp"
#include "Iterators.hpp"
#include "Queries.hpp"

using namespace std;
/**
    @file VpTree.hp
    In this File there is the definition of the VpTree class and all the necessary classes to perform queries
    on this data structure
*/

/**
    @class VpTree
    @param Value the type of the elements that the tree will hold
    @param Metric the metric function that will define the distance between two objects
    @param PivotSelector class templatized by Value and Metric
*/
template<typename Value, typename Metric, template<typename, typename> class PivotSelector>
class VpTree{
       /**
        @class VpNode
        The type of the VpTree nodes.
    */
    class VpNode{
        friend class VpTree;
    private:
        //const Value v;
        Value v;
        int size;
        typename Metric::image_type radius;
        Metric f;
        VpNode* right;
        VpNode* left;
    public:
        typedef Value value;
        typedef Value Element;
        VpNode(const Value _v, typename Metric::image_type _r): v(_v), radius(_r), right(0), left(0), size(1), f(Metric()){}
        ~VpNode(){
            delete right;
            delete left;
        }
        /**
            @param _v Object that we want to test
            return true if _v lies inside (or at the perimeter) of the ball defined by this node, false otherwise.
        */
        bool inside(const Value& _v){
            typename Metric::image_type r = f(v,_v);
            return r <= radius;
        }
        Value& getElement() {
            return this->v;
        }
        VpNode* getLeft(){
            return left;
        }
        VpNode* getRight(){
            return right;
        }
        typename Metric::image_type getRadius(){
            return radius;
        }
        
    };


    /*class VPIterator : public std::iterator<std::forward_iterator_tag, Value> {
        private:
            VpNode* current;
            stack<VpNode*> path;
        void begin(){
            while(current != 0){
                path.push(current);
                current = current->left;
            }
            if(not path.empty()) {
                current = path.top();
                path.pop();
                if(current->right != 0){
                    path.push(current->right);
                    VpNode* aux = current->right;
                    aux = aux->left;
                    while(aux != 0){
                        path.push(aux);
                        aux = aux->left;
                    }
                }
            }
        }
        public:
            VPIterator(VpNode* root, bool isBegin){
                if(isBegin) {
                    current = root;
                    begin();
                }else{
                    current = 0;
                }
            }
            ~VPIterator(){}
        
        Value& operator*(){
            return current->v;
        }
        Value& operator*() const {
            return current->v;
        }
        bool operator==(const VPIterator& vi) const{
            return current == vi.current;
        }
        bool operator!=(const VPIterator& vi) const{
            return not(*this == vi);
        }
        VPIterator& operator++(){
            if(not path.empty()){
                current = path.top();
                path.pop();
                if(current->right != 0){
                    path.push(current->right);
                    VpNode* aux = current->right;
                    aux = aux->left;
                    while(aux != 0){
                        path.push(aux);
                        aux = aux->left;
                    }
                }
            }else current = 0;
        }
    };*/

    //--------------------------------- Start defition VP-tree -------------------------------------------------
    typedef Value Element;
    VpNode* root;
    Metric f;
    //VpNode* _ghost;
    /**
        @param n node for which we want to know the size of the tree rooted at it
        Function to retrieve the size of a tree. This is just a wrapper to deal with the case of the empty
        tre (null pointer)
    */
    int nodeSize(VpNode* n){
        if(n == 0) return 0;
        else return n->size;
    }
    public:
        typedef NNQuery<Value> NNquery;
        //typedef VPIterator iterator;
        typedef Iterator<VpNode> iterator;
        typedef Iterator<const VpNode> const_iterator;
    /**
        @param c a vector holding the collection of elements
    */
    VpTree(vector<Element> c){
        f = Metric();
        root = build(c);
    }
    ~VpTree(){
        delete root;
    }
    iterator begin(){
        //return VPIterator(this->root, true);
        return iterator(this->root, true);
    }
    iterator end(){
        //return VPIterator(this->root, false);
        return iterator(this->root, false);
    }
    /**
        @param c vector holding the collection of elements that will be held in our tree
        returns a pointer to a VpTree constructed with the elements of c
    */
    VpNode* build(vector<Element> c){
        if(c.size() == 0) return 0;
        PivotSelector<Metric, Element> sel = PivotSelector<Metric,Element>();
        sel.process(c);
        VpNode* node = new VpNode(sel.getPivot(), sel.getRadius());
        node->left = build(sel.closerElements());
        node->right = build(sel.fartherElements());
        node->size = 1 + nodeSize(node->left) + nodeSize(node->right);
        return node;
    }
    void print(){
        print(this->root);
    }
    void print(VpNode* p, string tag = "root", int indent=0)
    {
        if(p == 0) cout << "hola" << endl;
        if(p != 0) {
            if (indent) {
                std::cout << std::setw(indent) << ' ' << tag;
            }
            cout<< p->v <<" " << p->radius << " \n ";
            if(p->left) {
                print(p->left, "Inner", indent+4);
            }
            if(p->right){
                print(p->right, "Outter", indent+4);
            }
        }
    }
    int getSize(){
        return this->root->size;
    }
    //----------------------------- Nearest Neighbour query ---------------------------------------
    
    /**
        @class NNSearcher
        This class is responsible for implementing the Hierarchical Ranking Search (HRS) for the nearest neighbour query
    */
    class NNSearcher{
        /**
            @class SearchElement
            The elements that will be pushed into the priority queue. It works as a wrapper for a pointer to a VpTree
            node and its score (distance in this case)
        */
        class SearchElement{
        public:
            typedef Value value;
            typename VpTree::VpNode* n; ///Pointer to the node
            typename Metric::image_type score; ///Score, in this case it's a distance
            bool isResult; ///Whether the element is of type 0 or 1
            SearchElement(){}
            //SearchElement(const Node* _n, bool _result, typename Metric::image_type _score, Key min=n->getKey(), Key max=n->getKey()):n(_n), isResult(_result),score(_score),box(min,max){}
            SearchElement(typename VpTree::VpNode* _n, bool _result, typename Metric::image_type _score): n(_n), isResult(_result),score(_score){}
            /**
                Comparison operator. Elements of type 0 have priority over type 1.
            */
            bool operator<(const SearchElement& e) const{
                if(isResult and e.isResult) return score > e.score;
                else return isResult;
            }
            bool notResult() const{
                return not isResult;
            }
            /**
                Method that retrieves the proper element from a pointer to a VpNode
            */
            Value getPayload(){
                return n->getElement();
            }
            /*SearchElement& operator=(SearchElement& el){
                n = el.n;
                box = el.box;
                score = el.score;
                isResult = el.isResult;
            }*/
        };
    public:
        Metric f;
        typedef SearchElement Element;
        typedef NNQuery<Value> Query;
        typedef QueryIterator<NNSearcher> qiterator;
        Query q;
        priority_queue<Element> pq;
        NNSearcher(){}
        NNSearcher(Query _q): q(_q), f(Metric()), pq(priority_queue<Element>()){}
        /**
            @param n A pointer to a node of the tree

            This method is used to initiate the search. It processes the root of the tree and pushes the required
            elements into the priority queue
        */
        void initFirst(typename VpTree::VpNode* n){
            if(n != 0) {
                Element first = Element(n, false, 0);
                pq.push(first);
                while(not pq.empty() and pq.top().notResult()){
                    Element x = pq.top();
                    pq.pop();
                    vector<Element> v = processElement(x);
                    for(typename vector<Element>::iterator it = v.begin(); it != v.end(); ++it) pq.push(*it);
                }
            }
        }
        /**
            @param x A Search element.

            Given a search element, generates the required elements, with the appropriate distances computed
        */
        vector<Element> processElement(Element& x){
            vector<Element> v = vector<Element>();
            Element root = Element(x.n, true, f(x.n->getElement(), q.v));
            v.push_back(root);
            typename Metric::image_type pivotDist = f(q.v, x.n->getElement());
            typename Metric::image_type r = x.n->getRadius();
            if(x.n->getRight() != 0) {
                typename Metric::image_type score = std::max(r-pivotDist, (typename Metric::image_type) 0);
                Element leftElem = Element(x.n->getRight(), false, score);
                v.push_back(leftElem);
            }
            if(x.n->getLeft() != 0) {
                typename Metric::image_type score = std::max(pivotDist-r, (typename Metric::image_type) 0);
                Element rightElem = Element(x.n->getLeft(), false, score);
                v.push_back(rightElem);
            }
            return v;
        }
        /**
            Interface method used by the QueryIterator. On invocation calculates the next type 0 element of the search
        */
        bool processNext(){
            if(not pq.empty()) pq.pop();
            while(not pq.empty() and pq.top().notResult()){
                Element x = pq.top();
                pq.pop();
                vector<Element> v = processElement(x);
                for(typename vector<Element>::iterator it = v.begin(); it != v.end(); ++it) pq.push(*it);
            }
            return not pq.empty();
        }
        /**
            Retrieve the last type 0 element
        */
        Element getLast(){
            return pq.top();
        }

        qiterator begin(){
            return qiterator(this, pq.empty());
        }

        qiterator end(){
            return qiterator(this, true);
        }
    };
    /**
        Specialized call to query for the Nearest neighbor search
    */
    NNSearcher query(NNQuery<Value> q){
        NNSearcher s = NNSearcher(q);
        s.initFirst(this->root);
        return s;
    }
};
#endif //_SMTL_VPTREE_H