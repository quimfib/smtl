#ifndef _RANDOM_H
#define _RANDOM_H

/**
	@file random.cpp
	Class borrowed from the Spatial and Metric template library
	of Maria Merce Pons Crepso, turned in fullfilment of the "Màster en Computació"
	of the Universitat Politècnica de Catalunya (UPC)
	This class gives random number generation functionalities
*/


#include <stdlib.h>
#include <time.h>

// ---------------------------------------------------------------------------
// Generacion de numeros aleatorios
// Uso:
// Random R; // creamos un generador de numeros aleatorios (GNA)
// int m = R(a,b); // devuelve un entero en el rango [a..b]
// int u = R(n);   // devuelve un entero en el rango [0..n-1]
// double x = R(); // devuelve un numero real entre 0 y 1
// Random R2(314159); // creamos un GNA inicializado con la
//                    // 'semilla' 314159
//
// Fijada una semilla el GNA siempre produce
// los mismos numeros aleatorios; esto es ventajoso si queremos
// reproducir la ejecucion de un algoritmo que usa un GNA todas las
// veces que queremos; cuando la semilla no se da al crear el GNA,
// se toma como semilla el tiempo que lleva el computador encendido
// lo que nos da una impresion de aletoriedad: diferentes
// ejecuciones, usaran semillas distintas (= tiempos distintos) y
// los numeros aleatorios generados en cada caso seran diferentes.
//
// Se implementa mediante el metodo desarrollado por D. E. Knuth para la
// Stanford GraphBase

class BadRangeRandomInteger{};


class Random
{
public:
	
	Random(long seed = time(0));
	long operator()(long a, long b) throw (BadRangeRandomInteger);
	long operator()(long n) throw (BadRangeRandomInteger);
	double operator()() throw();

private:
	long A[56];
	long* fptr;
	
	void init_rand(long);
	long cycle();
	inline long rand()
	{
		return *fptr >= 0 ? *fptr-- : cycle();
	}
};


#define TWOP31 ((unsigned long) 0x80000000)

double Random::operator()() throw()
{
	return double(Random::rand()) / double(TWOP31);
};

long Random::operator()(long a, long b) throw (BadRangeRandomInteger)
{
	if (a > b || a < 0)
		throw BadRangeRandomInteger();
	return operator()(b - a + 1) + a;
};

long Random::operator()(long n) throw (BadRangeRandomInteger)
{
	if (n <= 0)
		throw BadRangeRandomInteger();
	register unsigned long t = TWOP31 - (TWOP31 % n);
	register long r;
	do
	{
		r = Random::rand();
	}while (t <= (unsigned long) r);
	return r % n;
};

Random::Random(long int seed)
{
	A[0] = -1;
	for (int i = 1; i<= 55; ++i)
		A[i] = 0;
	init_rand(seed);
};


// difference modulo 2^31
#define mod_diff(x,y) (((x) - (y)) & 0x7fffffff)

long Random::cycle()
{
	int i,j;
	for (i = 1, j = 32; j < 56; ++i, ++j)
		A[i] = mod_diff(A[i], A[j]);
	for (j = 1; i < 56; ++i, ++j)
		A[i] = mod_diff(A[i], A[j]);
	fptr = &A[54];
	return A[55];
};


void Random::init_rand(long seed)
{
	register long i;
	register long prev = seed, next = 1;
	seed = prev = mod_diff(prev,0);
	A[55] = prev;
	for (i = 21; i; i = (i + 21) % 55)
	{
		A[i] = next;
		next = mod_diff(prev,next);
		if (seed & 1) 
			seed = 0x40000000 + (seed >> 1);
		else 
			seed >>= 1;
		next = mod_diff(next,seed);
		prev = A[i];
	}
	cycle();cycle();cycle();cycle();
};

Random R;

#endif	//_RANDOM_H