#ifndef _SMTL_ITERATOR_H
#define _SMTL_ITERATOR_H
#include <utility>
#include <list>
#include <vector>

using namespace std;

template<typename Node>
    class Iterator {
        private:
            Node* current;
            const Node* end;
            list<Node*> forward;
            list<Node*> backward;

            void init(Node* n, Node* root){
                Node* aux = root;
                const typename Node::Key_type k  = n->elem.first;
                while(aux != n){
                    if(aux->elem.first[aux->discriminant] < k[aux->discriminant]){
                        forward.push_back(aux);
                        aux = aux->left;
                    }else if(aux->elem.first[aux->discriminant] > k[aux->discriminant]){
                        backward.push_back(aux);
                        aux = aux->right;
                    }
                }
                current = aux;
            }
            void init_end(Node* n, Node* root){
                Node* aux = root;
                while(aux != end){
                    backward.push_back(aux);
                    aux = aux->right;
                }
                current = aux;
            }
        public:
            Iterator(Node* n, const Node* _end){
                current = n;
                forward = list<Node*>();
                backward = list<Node*>();
                end = _end;
            }
            Iterator(Node* n, bool b){
                current = n;
                forward = list<Node*>();
                backward = list<Node*>();
                end = 0;
            }
            Iterator(Node* n, const Node* _end, list<Node*> fwr, list<Node*> bckwd){
                current = n;
                forward = fwr;
                backward = bckwd;
                end = _end;
            }
            Iterator(Node* n, Node* root, const Node* _end){
                forward = list<Node*>();
                backward = list<Node*>();
                end = _end;
                if(n == end){
                    init_end(n,root);
                }else init(n,root);
            }
            ~Iterator(){}

            typename Node::Element& operator*(){
                return current->elem;
            }

            const typename Node::Element& operator*() const{
                return current->elem;
            }
            Iterator operator++(){
                if(current != end) {
                    current = forward.pop_back();
                    Node* aux = current->right;
                    if(aux != 0) {
                        backward.push_back(current);
                        forward.push_back(aux);
                        while(aux->left != 0) {
                            forward.push_back(aux);
                            aux = aux->left;
                        }
                    }
                    return *this;
                }
            }
            Iterator operator--(){
                if(current->left != 0 or not backward.empty()){
                    current = backward.pop_back();
                    Node* aux = current->left;
                    if(aux != 0) {
                        backward.push_back(aux);
                        while(aux->left != 0) {
                            backward.push_back(aux);
                            aux = aux->left;
                        }
                    }
                }
                return *this;
            }
            bool operator==(Iterator<Node> it) const {
                return this->current == it.current;
            }
            bool operator!=(Iterator<Node> it) const {
                return not(this->current == it.current);
            }
        };
#endif //_SMTL_ITERATOR_H