#ifndef _SMTL_CONTAINERS_H
#define _SMTL_CONTAINERS_H

//#include <priority_queue>
#include <stack>
#include <queue>
#include <vector>

using namespace std;

template<typename Value>
class PriorityQueue{
    priority_queue<Value> pq;
public:
    PriorityQueue(){
        pq = priority_queue<Value>();
    }
    ~PriorityQueue(){}
    Value retrieve(){
        return pq.top();
    }
    void pop(){
        pq.pop();
    }
    void push(Value v){
        pq.push(v);
    }
    bool operator==(PriorityQueue& _pq){
        return pq == _pq.pq;
    }
    bool operator!=(PriorityQueue& _pq){
        return not(*this == _pq);
    }
    PriorityQueue& operator=(PriorityQueue& _pq){
        pq = _pq;
    }
    bool empty(){
        return pq.empty();
    }
};

template<typename Value>
class Queue{
    queue<Value> q;
public:
    Queue(){
        q = queue<Value>();
    }
    ~Queue(){}
    Value retrieve(){
        return q.top();
    }
    void pop(){
        q.pop();
    }
    void push(Value v){
        q.push(v);
    }
    bool operator==(Queue& _q){
        return q == _q.q;
    }
    bool operator!=(Queue& _q){
        return not(*this == _q);
    }
    Queue& operator=(Queue& _q){
        q = _q;
    }
    bool empty(){
        return q.empty();
    }
};

template<typename Value>
class Stack{
    stack<Value> s;
public:
    Stack(){
        s = stack<Value>();
    }
    ~Stack(){}
    Value retrieve(){
        return s.top();
    }
    void pop(){
        s.pop();
    }
    void push(Value v){
        s.push(v);
    }
    bool operator==(Stack& _s){
        return s == _s.s;
    }
    bool operator!=(Stack& _s){
        return not(*this == _s);
    }
    Stack& operator=(Stack& _s){
        s = _s;
    }
    bool empty(){
        return s.empty();
    }
};
#endif //_SMTL_CONTAINERS_H