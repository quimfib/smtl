#ifndef _SMTL_BASIC_KEY_H
#define _SMTL_BASIC_KEY_H

#include <vector>

using namespace std;
/**
    @file BasicKey.hpp
    This file gives an implementation of a multicomponent key, suitable for use with the SMTL
*/

/**
    @class BasicKey
    @param Value Type of the key components
    @param Dim Dimension of the key. Optional paramater. If not specified, will take the dimension
            of the vector given through the constructor
    This class just implements a multicomponenet key as a vector. Allows for comparison between components, random access
    through the [] operator.
*/
template<typename Value, int Dim = 0>
class BasicKey{
    private:
        int dimension;
    public:
        typedef Value key_comp;
        vector< Value > key;
    /**
        @param _key a vector holding all the key's components
    */
    BasicKey(vector< Value > _key){
        key = _key;
        if(Dim == 0) dimension = _key.size();
        else dimension = Dim;
    }
    BasicKey(){
        key = vector<Value>(Dim);
        dimension = Dim;
    }
    BasicKey(bool b){}
    BasicKey<Value>& operator=(BasicKey<Value>& _hk){
        key = _hk.key;
    }
    Value& operator[](int i){
        return key[i];
    }
    const Value& operator[](int i) const{
        return key[i];
    }
    bool operator==(const BasicKey<Value>& _hk) const{
        if(_hk.getDimension() != dimension) return false;
        typename vector<Value>::const_iterator it = key.begin();
        typename vector<Value>::const_iterator itp = _hk.key.begin();
        while(it != key.end() and itp != _hk.key.end()) {
            if(*it != *itp) return false;
            ++it;
            ++itp;
        }
        return true;
    }
    bool operator!=(BasicKey<Value>& _hk) const{
        return !(*this == _hk);
    }
    int getDimension() const{
        return dimension;
    }
    int size() const{
        return dimension;
    }
};
#endif //_SMTL_BASIC_KEY_H