#ifndef _SMTL_KDTREE_H
#define _SMTL_KDTREE_H
#include <utility>
#include <list>
#include <vector>
#include "random.cpp"
#include "Selectors.hpp"
#include <algorithm>
#include <iostream>
#include <iomanip>
#include <stack>
#include <queue>
#include "HyperRectangle.hpp"
#include "Queries.hpp"
#include "Iterators.hpp"

using namespace std;

/**
    @file KdTree.hpp
    In this File there is the definition of the KdTree class and all the necessary classes to perform queries
    on this data structure
*/

//namespace smtl{

/**
    @class KdTree
    @param Dim dimension of the keys stored in this kdtree
    @param Key the type of the elements that will be used as key
    @param Value the type of the elements that the tree will hold 
    @param Selector templatized class that will define how to insert and delete elements
*/
template <int Dim, typename Key, typename Value, template<typename, int> class Selector>
class KdTree {
private:
    typedef Key _Key;
    typedef Value _Value;
//----------------------------- PRIVATE MEMBERS ---------------------------------------------------------------

    /**
        @class KdNode
        Definition of the kdtree nodes. It is just an Element, enriched with
        discriminant, size and pointers to both children
    */
    class KdNode {
        friend class KdSearch;
        friend class KdTree;
        public: 
            typedef typename KdTree::_Key Key_type;
            typedef typename KdTree::_Value Value_type;

            //typedef pair<const Key_type, Value_type> Element;
            
            /*WARNING:
                Key_type define as non-const type to avoid compile issues with the remove
                functionality. Beware that this allows for the keys to be modified
            */

            typedef pair<Key_type, Value_type> Element;
            //typedef const Element const_Element;
        private:
            Element elem;
            int discriminant;
            int size;
            KdNode* left;
            KdNode* right;
        public:
            typedef Element value;
            KdNode(Key nKey, Value nValue, int disc): elem(make_pair(nKey, nValue)), discriminant(disc), size(1),
                                                    left(0), right(0){}
            KdNode(){}
            ~KdNode(){
                delete left;
                delete right;
            }
            Key getKey(){
                return this->elem.first;
            }
            Value getValue(){
                return this->elem.second;
            }
            Element& getElement(){
                return this->elem;
            }
            KdNode* getLeft(){
                return left;
            }
            KdNode* getRight(){
                return right;
            }
            int getDiscriminant(){
                return discriminant;
            }
    };

    /*class KdIterator {
        private:
            KdNode* current;
            stack<KdNode*> path;
        void find(const Key& k){    
            bool found = false;
            int i = 0;
            while(current != 0 and not found) {
                i = current->discriminant;
                if(k[i] < current->getKey()[i]) {
                    path.push(current);
                    current = current->left;
                }else if (k[i] > current->getKey()[i]) {
                    current = current->right;
                }else{
                    found = k == current->getKey();
                    if(not found) current = current->right;
                }
            }  
        }
        void begin(){
            while(current != 0){
                path.push(current);
                current = current->left;
            }
            if(not path.empty()) {
                current = path.top();
                path.pop();
                if(current->right != 0){
                    path.push(current->right);
                    KdNode* aux = current->right;
                    aux = aux->left;
                    while(aux != 0){
                        path.push(aux);
                        aux = aux->left;
                    }
                }
            }
        }
        public:
            KdIterator(const Key& _k, KdNode* root) {
                current = root;
                find(_k);
            }
            KdIterator(KdNode* root, bool isBegin){
                if(isBegin) {
                    current = root;
                    begin();
                }else{
                    current = 0;
                }
            }
            ~KdIterator(){}
        
        typename KdNode::Element& operator*(){
            return current->elem;
        }
        typename KdNode::Element& operator*() const {
            return current->elem;
        }
        bool operator==(const KdIterator& ki) const{
            return current == ki.current;
        }
        bool operator!=(const KdIterator& ki) const{
            return not(*this == ki);
        }
        KdIterator& operator++(){
            if(not path.empty()){
                current = path.top();
                path.pop();
                if(current->right != 0){
                    path.push(current->right);
                    KdNode* aux = current->right;
                    aux = aux->left;
                    while(aux != 0){
                        path.push(aux);
                        aux = aux->left;
                    }
                }
            }else current = 0;
        }
    };*/
//-----------------------------Start KdTree definition--------------------------------------------------------------

    typedef typename KdNode::Element Element;
    KdNode* root;
    //KdNode* _ghost;
    /**
        @param n node for which we want to know the size of the tree rooted at it
        Function to retrieve the size of a tree. This is just a wrapper to deal with the case of the empty
        tre (null pointer)
    */
    int nodeSize(KdNode* n){
        if(n == 0) return 0;
        else return n->size;
    }
    /**
        @param n a pointer to a KdNode
        Returns all elements of the tree rooted at n randomly shuffled
    */
    vector<Element> getSubtreesElems(KdNode* n){
        vector<Element> elems = getItems(n->left);
        vector<Element> elemsR = getItems(n->right);
        elems.insert(elems.end(),elemsR.begin(), elemsR.end());
        random_shuffle(elems.begin(), elems.end());
        return elems;
    }
    /**
        @param elems vector of elements
        reinserts all elements in elems into the tree via the insert method
    */
    void reinsertChildren(vector<Element> elems){
        for(typename vector<Element>::iterator it = elems.begin(); it != elems.end(); ++it) insert(*it);
    }
    /**
        @param key key of the element to be removed
        @param node current node being explored
        @param sel selector object
        Removes the element with "key" key and rebuilds the tree as indicated by the deleteRoot method of sel
    */
    void remove(const Key& key, KdNode* node, Selector<Key, Dim>& sel){
        KdNode* current = node;
        KdNode* previous = 0;
        list<KdNode*> visited = list<KdNode*>();
        bool found = false;
        bool lastLeft = false;
        int i = 0;
        while(current != 0 and not found) {
            i = current->discriminant;
            if(key[i] < current->getKey()[i]) {
                lastLeft = true;
                previous = current;
                visited.push_back(previous);
                current = current->left;
            }else if (key[i] > current->getKey()[i]) {
                lastLeft = false;
                previous = current;
                visited.push_back(previous);
                current = current->right;
            }else{
                found = key == current->getKey();
                if(not found) current = current->right;
            }
        }
        if(found) {
            if(previous != 0){
                if(lastLeft) previous->left = 0;
                else previous->right = 0;
            }
            vector<Element> elems = getSubtreesElems(current);
            if(not sel.deleteRoot()) {
                int sizeDeleted = elems.size();
                while(not visited.empty()){
                    KdNode* aux = visited.front();
                    visited.pop_front();
                    aux->size -= sizeDeleted;
                }
                reinsertChildren(elems);
            }else {
                if(previous != 0){
                    if(lastLeft) previous->left = delete_root(current);
                    else previous->right = delete_root(current);
                    //previous->size = 1 + nodeSize(previous->getLeft()) + nodeSize(previous->getRight());
                    while(not visited.empty()){
                        KdNode* aux = visited.back();
                        visited.pop_back();
                        aux->size = 1 + nodeSize(aux->getLeft()) + nodeSize(aux->getRight());
                    }
                }
            }
        }
    }
    /**
        @param t pointer to the KdNode that we want to split
        @param k Key that we will use as reference to split
        @param i discriminant we will use to retrieve the splitting value from k

        Splits a relaxed randomized kd tree and returns a pair with two relaxed randomized trees.
        Any key k1 in return.first complies k1[i] < k[i] and any key k2 in return.second complies
        k2[i] > k[i]
    */
    pair<KdNode*,KdNode*> split(KdNode* t, const Key& k, int i){
        KdNode* p = 0;
        KdNode* q = 0;
        if(t == 0) return make_pair(p,q);
        int j = t->discriminant;
        if(i == j) {
            if(k[i] < t->getKey()[i]){
                pair<KdNode*,KdNode*> result = split(t->getLeft(), k, i);
                t->left = result.second;
                p = result.first;
                q = t;
            }else{
                pair<KdNode*,KdNode*> result = split(t->getRight(), k, i);
                t->right = result.first;
                p = t;
                q = result.second;
            }
        }else{
            pair<KdNode*,KdNode*> L = split(t->getLeft(), k, i);
            pair<KdNode*,KdNode*> R = split(t->getRight(), k ,i);
            if(k[i] < t->getKey()[i]) {
                t->left = L.second;
                t->right = R.second;
                p = join(L.first, R.first, j);
                q = t;
            }else{
                t->left = L.first;
                t->right = R.first;
                p = t;
                q = join(L.second, R.second, j);
            }
        }
        if(p != 0) p->size = 1 + nodeSize(p->getLeft()) + nodeSize(p->getRight());
        if(q != 0) q->size = 1 + nodeSize(q->getLeft()) + nodeSize(q->getRight());
        return make_pair(p,q);
    }
    /**
        @param U a pointer to a KdNode
        @param V a pointer to a KdNode
        @param i integer, that represents the integer that we will use to join U and V
        
        Joins U and V into a single relaxed randomized tree whose root splits by the i coordinate
    */
    KdNode* join(KdNode* U, KdNode* V, int i){
        if(U == 0) return V;
        if(V == 0) return U;

        int m = U->size;
        int n = V->size;
        Random R;
        if(R(m+n-1) < m){
            int j = U->getDiscriminant();
            if(i == j) {
                U->right = join(U->getRight(), V, i);
            }else {
                pair<KdNode*,KdNode*> result = split(V,U->getKey(),j);
                U->left = join(U->getLeft(), result.first, i);
                U->right = join(U->getRight(), result.second, i);
            }
            U->size = 1 + nodeSize(U->getLeft()) + nodeSize(U->getRight());
            return U;
        }else{
            int j = V->getDiscriminant();
            if(i == j) {
                V->left = join(V->getLeft(), U, i);
            }else {
                pair<KdNode*,KdNode*> result = split(U,V->getKey(),j);
                V->left = join(V->getLeft(), result.first, i);
                V->right = join(V->getRight(), result.second, i);
            }
            V->size = 1 + nodeSize(V->getLeft()) + nodeSize(V->getRight());
            return V;
        }
    }
    /**
        @param T a pointer to a KdNode
        @param k the key that we want to insert
        @param v the associated value that we want to insert
        @param discriminant the discriminant by which the new node is going to split

        Returns a node that has <Key, Value> and splits by position = discriminant. If T
        is non-null then splits the tree rooted at T and appends it as children
    */
    KdNode* insert_at_root(KdNode* T, const Key& k, const Value& v, int discriminant){
        KdNode* r = new KdNode(k,v,discriminant);
        pair<KdNode*,KdNode*> result = split(T,k,discriminant);
        r->left = result.first;
        r->right = result.second;
        r->size = 1 + nodeSize(r->left) + nodeSize(r->right);
        KdNode* l = r->left;
        KdNode* ri = r->right;
        return r;
    }
    /**
        @param T node that we want to delete

        Deletes the node and returns the join of its children
    */
    KdNode* delete_root(KdNode* T){
        KdNode* U = T->left;
        KdNode* V = T->right;
        int i = T->discriminant;
        T->left = 0;
        T->right = 0;
        delete T;
        return join(U,V,i);
    }
    /**
        @param key key to be inserted
        @param v value to be inserted
        @param current current node being explored
        @param sel selector being used

        Returns a KdNode that holds <key,v> as an element and respect the invariants of the tree.
    */
    KdNode* insertWithSelector(const Key& key, const Value& v, KdNode* current, Selector<Key, Dim>& sel){
        if(current == 0) return insert_at_root(current, key, v, sel.calculateDiscriminant()); 
        NextOp op = sel.next(key, current->getKey(), current->size, current->discriminant);
        if(op == LEFT){
            current->left = insertWithSelector(key,v,current->left, sel);
            current->size = 1 + nodeSize(current->left) + nodeSize(current->right);
        }else if(op == RIGHT) {
            current->right = insertWithSelector(key,v,current->right, sel);
            current->size = 1 + nodeSize(current->left) + nodeSize(current->right);
        }else if(op == INSERT){
            current = insert_at_root(current, key, v, sel.calculateDiscriminant());
        }
        return current;
    }

//----------------------------- PUBLIC MEMBERS ---------------------------------------------------------------
public:
    /*template <typename Iterator>
    KdTree(Iterator begin, Iterator end);*/

    typedef IteratorFind<KdNode, Key> iterator;
    typedef NNQuery<Key> NNquery;
    typedef RangeQuery<Key> Rangequery;
    typedef PMatchQuery<Key> PMquery;
    typedef KdNode node_type;
    typedef Key key_type;
    typedef Value mapped_type;
    typedef typename KdNode::Element element_type;
    KdTree(){
        this->root = 0;
        //this->_ghost = new KdNode();
    }
    ~KdTree(){
        if(root != 0) delete root;
        //delete _ghost;
    }
    /**
        @param n pointer to a KdNode

        Returns all the elements of the tree rooted at n
    */
    vector<Element> getItems(KdNode* n){
        if(n == 0) return vector<Element>();
        vector<Element> elems = getItems(n->left);
        vector<Element> elemsR = getItems(n->right);
        elems.push_back(make_pair(n->elem.first,n->elem.second));
        elems.insert(elems.end(),elemsR.begin(), elemsR.end());
        return elems;
    }

    iterator begin(){
        return iterator(this->root, true);
    }

    iterator end(){
        return iterator(this->root, false);
    }
    /**
        @param key key to be inserted
        @param v value to be inserted

        Inserts a node holding <key,v> into the tree
    */
    void insert(const Key& key, const Value& v ){
        Selector<Key, Dim> sel = Selector<Key, Dim>();
        if(this->root == 0) {
            this->root = new KdNode(key, v, sel.calculateDiscriminant());
            //this->root->right = this->_ghost;
        }else {
            this->root = insertWithSelector(key, v, this->root, sel);
        }
    }

    /**
        @param begin
        @param end

        Given an iterator, inserts all elements between being and end into the tree
    */
    template <typename Iterator>
    void insert(Iterator begin, Iterator end){
        for(Iterator it = begin; it != end; ++it){
            insert(*it);
        }
    }
    /**
        @param elem pair <key,value>

        Insertes <key,value> into the tree
    */
    void insert(Element& elem){
        insert(elem.first, elem.second);
    }
    /**
        @param key key to be searched in the tree

        Returns an iterator pointin to the node that has key as key. Returns end iterator if not found
    */
    iterator find(const Key& key){
        return iterator(key, this->root);
    }
    /**
        @param key key to be removed

        removes the element with key key.
    */
    void remove(const Key& key){
        Selector<Key, Dim> sel = Selector<Key, Dim>();
        remove(key, this->root, sel);
    }
    void printRoot(){
        cout << this->root->size << " " << this->root->elem.second << endl;
    }
    void print(){
        print(this->root);
    }
    void print(KdNode* p, string tag = "root", int indent=0)
    {
        if(p == 0) cout << "hola" << endl;
        if(p != 0) {
            if (indent) {
                std::cout << std::setw(indent) << ' ' << tag;
            }
            cout<< p->elem.second << " discriminant " << p->discriminant << " size " << p->size <<" \n ";
            if(p->left) {
                print(p->left, "Left ", indent+4);
            }
            if(p->right){
                print(p->right, "Right ", indent+4);
            }
        }
    }
    int getDimension(){
        return this->root->getDimension();
    }
    int getSize(){
        return this->root->size;
    }

//-------------------------------QUERY SECTION ------------------------------------------------

//------------------------------ GENERAL QUERY DEFINITION ------------------------------------

    /**
        @class NNSearcher
        This class is responsible for implementing the Hierarchical Ranking Search (HRS) for the nearest neighbour query
    */
    template<typename Metric>
    class NNSearcher{
        typedef HyperRectangle<Key, Dim> BBox;
        /**
            @class SearchElement
            The elements that will be pushed into the priority queue. It works as a wrapper for a pointer to a KdTree
            node, its score (distance in this case) and a bounding box of the whole tree rooted at the ponter
        */
        class SearchElement{
        public:
            typedef typename KdNode::Element value;
            typename KdTree::KdNode* n; ///Pointer to the node
            BBox box; ///Bounding box
            typename Metric::image_type score; ///Score, in this case it's a distance
            bool isResult; ///Whether the element is of type 0 or 1
            SearchElement(){}
            //SearchElement(const Node* _n, bool _result, typename Metric::image_type _score, Key min=n->getKey(), Key max=n->getKey()):n(_n), isResult(_result),score(_score),box(min,max){}
            SearchElement(typename KdTree::KdNode* _n, bool _result, typename Metric::image_type _score, BBox _box): n(_n), isResult(_result),score(_score),box(_box){}
            /**
                Comparison operator. Elements of type 0 have priority over type 1.
            */
            bool operator<(const SearchElement& e) const{
                if(isResult and e.isResult) return score > e.score;
                else return isResult;
            }
            bool notResult() const{
                return not isResult;
            }
            /**
                Method that retrieves the proper element from a pointer to a GHNode
            */
            typename KdTree::KdNode::Element getPayload(){
                return n->getElement();
            }
            /*SearchElement& operator=(SearchElement& el){
                n = el.n;
                box = el.box;
                score = el.score;
                isResult = el.isResult;
            }*/
        };
    public:
        Metric f;
        typedef SearchElement Element;
        typedef NNQuery<Key> Query;
        typedef QueryIterator<NNSearcher> qiterator;
        Query q;
        priority_queue<Element> pq;
        NNSearcher(){}
        NNSearcher(Query _q): q(_q), f(Metric()), pq(priority_queue<Element>()){}
        /**
            @param n A pointer to a node of the tree

            This method is used to initiate the search. It processes the root of the tree and pushes the required
            elements into the priority queue
        */
        void initFirst(typename KdTree::KdNode* n){
            if(n != 0) {
                BBox box = BBox(q.min,q.max);
                Element first = Element(n, false, 0, box);
                pq.push(first);
                while(not pq.empty() and pq.top().notResult()){
                    Element x = pq.top();
                    pq.pop();
                    vector<Element> v = processElement(x);
                    for(typename vector<Element>::iterator it = v.begin(); it != v.end(); ++it) pq.push(*it);
                }
            }
        }
        /**
            @param x A Search element.

            Given a search element, generates the required elements, with the appropriate distances computed
        */
        vector<Element> processElement(Element& x){
            vector<Element> v = vector<Element>();
            Element root = Element(x.n, true, f(x.n->getKey(), q.v), x.box);
            BBox box = x.box;
            v.push_back(root);
            int d = x.n->getDiscriminant();
            if(x.n->getLeft() != 0){
                BBox leftBox = box;
                leftBox.upper[d] = x.n->getKey()[d];
                //typename Metric::image_type dist = leftBox.distToPoint<Metric>(q.v);
                typename Metric::image_type dist = leftBox.distToPoint(q.v, Metric());
                Element leftElem = Element(x.n->getLeft(), false, dist, leftBox);
                v.push_back(leftElem);  
            }
            if(x.n->getRight() != 0){
                BBox rightBox = box;
                rightBox.lower[d] = x.n->getKey()[d];
                //typename Metric::image_type dist = rightBox.distToPoint<Metric>(q.v);
                typename Metric::image_type dist = rightBox.distToPoint(q.v, Metric());
                Element rightElem = Element(x.n->getRight(), false, dist, rightBox);
                v.push_back(rightElem); 
            }
            return v;
        }
        /**
            Interface method used by the QueryIterator. On invocation calculates the next type 0 element of the search
        */
        bool processNext(){
            if(not pq.empty()) pq.pop();
            while(not pq.empty() and pq.top().notResult()){
                Element x = pq.top();
                pq.pop();
                vector<Element> v = processElement(x);
                for(typename vector<Element>::iterator it = v.begin(); it != v.end(); ++it) pq.push(*it);
            }
            return not pq.empty();
        }
        /**
            Retrieve the last type 0 element
        */
        Element getLast(){
            return pq.top();
        }

        qiterator begin(){
            return qiterator(this, pq.empty());
        }

        qiterator end(){
            return qiterator(this, true);
        }
    };
    /**
        Specialized call to query for the Nearest neighbor search
    */
    template<typename Metric>
    NNSearcher<Metric> query(NNQuery<Key> q){
        NNSearcher<Metric> s = NNSearcher<Metric>(q);
        s.initFirst(this->root);
        return s;
    }
//----------------------------------------------- RANGE SEARCH --------------------------------------------
    /**
        @class RangeSearcher
        This class is responsible for implementing the DFS/BFS search as a range query
    */
    class RangeSearcher{
        /**
            @class SearchElement
            The elements that will be pushed into the stack/queue. It works as a wrapper for a pointer to a KdTree
            node.
        */
        class SearchElement{
        public:
            typedef typename KdNode::Element value;
            typename KdTree::KdNode* n; ///Pointer to a KdTree
            SearchElement(){}
            //SearchElement(const Node* _n, bool _result, typename Metric::image_type _score, Key min=n->getKey(), Key max=n->getKey()):n(_n), isResult(_result),score(_score),box(min,max){}
            SearchElement(typename KdTree::KdNode* _n): n(_n){}
            /**
                Method that retrieves the proper element from a pointer to a GHNode
            */
            typename KdTree::KdNode::Element getPayload(){
                return n->getElement();
            }
        };
    public:
        typedef SearchElement Element;
        typedef RangeQuery<Key> Query;
        typedef QueryIterator<RangeSearcher> qiterator;
        Query q;
        stack<Element> s;
        Element last;
        bool found;
        RangeSearcher(){}
        RangeSearcher(Query _q): q(_q), s(stack<Element>()), last(Element()), found(false){}
        /**
            @param n A pointer to a node of the tree

            This method is used to initiate the search. It processes the root of the tree and pushes the required
            elements into the priority queue
        */
        void initFirst(typename KdTree::KdNode* n){
            if(n != 0) {
                Element first = Element(n);
                s.push(first);
                while(not s.empty() and not found){
                    Element x = s.top();
                    s.pop();
                    if(x.n != 0){
                        if(q.match(x.n->getElement().first)) {
                            last = x;
                            found = true;
                        }
                        vector<Element> v = processElement(x);
                        for(typename vector<Element>::iterator it = v.begin(); it != v.end(); ++it) s.push(*it);
                    }
                }
            }
        }
        /**
            @param x A Search element.

            Given a search element, generates the required elements, with the appropriate distances computed
        */
        vector<Element> processElement(Element& x){
            vector<Element> v = vector<Element>();
            int d = x.n->getDiscriminant();
            if(x.n->getKey()[d] < q.upper[d]) v.push_back(Element(x.n->getRight()));
            if(x.n->getKey()[d] > q.lower[d]) v.push_back(Element(x.n->getLeft()));
            return v;
        }
        /**
            Interface method used by the QueryIterator. On invocation calculates the next type 0 element of the search
        */
        bool processNext(){
            bool hasNext = false;
            while(not s.empty() and not hasNext){
                Element x = s.top();
                s.pop();
                if(x.n != 0){
                    if(q.match(x.n->getElement().first)) {
                        last = x;
                        hasNext = true;
                    }
                    vector<Element> v = processElement(x);
                    for(typename vector<Element>::iterator it = v.begin(); it != v.end(); ++it) s.push(*it);
                }
            }
            return hasNext;
        }
        /**
            Retrieve the last type 0 element
        */
        Element getLast(){
            return last;
        }

        qiterator begin(){
            return qiterator(this, not found);
        }

        qiterator end(){
            return qiterator(this, true);
        }
    };
    /**
        Specialized call to query for the range search
    */
    RangeSearcher query(RangeQuery<Key> q){
        RangeSearcher s = RangeSearcher(q);
        s.initFirst(this->root);
        return s;
    }
//--------------------------------- Partial match -----------------------------------------------------------
    /**
        @class PMatchSearcer
        This class is responsible for implementing the DFS/BFS search as a partial match query
    */
    class PMatchSearcer{
        /**
            @class SearchElement
            The elements that will be pushed into the stack/queue. It works as a wrapper for a pointer to a KdTree
            node.
        */
        class SearchElement{
        public:
            typedef typename KdNode::Element value;
            typename KdTree::KdNode* n; ///Pointer to a KdTree node
            SearchElement(){}
            //SearchElement(const Node* _n, bool _result, typename Metric::image_type _score, Key min=n->getKey(), Key max=n->getKey()):n(_n), isResult(_result),score(_score),box(min,max){}
            SearchElement(typename KdTree::KdNode* _n): n(_n){}
            /**
                Method that retrieves the proper element from a pointer to a GHNode
            */
            typename KdTree::KdNode::Element getPayload(){
                return n->getElement();
            }
        };
    public:
        typedef SearchElement Element;
        typedef PMatchQuery<Key> Query;
        typedef QueryIterator<PMatchSearcer> qiterator;
        Query q;
        stack<Element> s;
        Element last;
        bool found;
        PMatchSearcer(){}
        PMatchSearcer(Query _q): q(_q), s(stack<Element>()), last(Element()), found(false){}
        /**
            @param n A pointer to a node of the tree

            This method is used to initiate the search. It processes the root of the tree and pushes the required
            elements into the priority queue
        */
        void initFirst(typename KdTree::KdNode* n){
            if(n != 0) {
                Element first = Element(n);
                s.push(first);
                while(not s.empty() and not found){
                    Element x = s.top();
                    s.pop();
                    if(x.n != 0){
                        if(q.match(x.n->getElement().first)) {
                            last = x;
                            found = true;
                        }
                        vector<Element> v = processElement(x);
                        for(typename vector<Element>::iterator it = v.begin(); it != v.end(); ++it) s.push(*it);
                    }
                }
            }
        }
        /**
            @param x A Search element.

            Given a search element, generates the required elements, with the appropriate distances computed
        */
        vector<Element> processElement(Element& x){
            vector<Element> v = vector<Element>();
            int d = x.n->getDiscriminant();
            if(q.relevant[d]){
                if(x.n->getKey()[d] < q.v[d]) v.push_back(Element(x.n->getRight()));
                else if(x.n->getKey()[d] > q.v[d]) v.push_back(Element(x.n->getLeft()));
            }else{
                v.push_back(Element(x.n->getRight()));
                v.push_back(Element(x.n->getLeft()));
            }
            return v;
        }
        /**
            Interface method used by the QueryIterator. On invocation calculates the next type 0 element of the search
        */
        bool processNext(){
            bool hasNext = false;
            while(not s.empty() and not hasNext){
                Element x = s.top();
                s.pop();
                if(x.n != 0){
                    if(q.match(x.n->getElement().first)) {
                        last = x;
                        hasNext = true;
                    }
                    vector<Element> v = processElement(x);
                    for(typename vector<Element>::iterator it = v.begin(); it != v.end(); ++it) s.push(*it);
                }
            }
            return hasNext;
        }
        /**
            Retrieve the last type 0 element
        */
        Element getLast(){
            return last;
        }

        qiterator begin(){
            return qiterator(this, not found);
        }

        qiterator end(){
            return qiterator(this, true);
        }
    };
    /**
        Specialized call to query for the partial match search
    */
    PMatchSearcer query(PMatchQuery<Key> q){
        PMatchSearcer s = PMatchSearcer(q);
        s.initFirst(this->root);
        return s;
    }
    
};
//}// end namespace smtl
#endif //_SMTL_STD_KDTREE_H
