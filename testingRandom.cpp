#include <string>
#include <iostream>
#include <utility>
#include <vector>
#include "KdTree.hpp"
#include "BasicKey.hpp"
#include <typeinfo>
#include "minkowski.hpp"
#include "Containers.hpp"
#include <limits>

using namespace std;
using namespace smtl;

class Test{
public:
    int valor;
    Test(){}
    Test(int v): valor(v){}
    void print(){
        cout << valor << endl;
    }
};

int main(){
    /*BasicKey<int> k1 = BasicKey<int>(vector<int>(23,33));
    BasicKey<int> k2 = BasicKey<int>(vector<int>(23,33));

    k1[1] = 101;
    if(k1 != k2) cout << "Hola";
    else cout << "Adeu";
    cout << endl;

    cout << k1[23] << endl;*/
    //typedef RelaxedRandomizedSelector RelaxedRandomizedSelector;
    typedef KdTree<2,BasicKey<int>,double, RelaxedRandomizedSelector> kdtree;
    KdTree<2,BasicKey<int>,double, RelaxedRandomizedSelector> t = KdTree<2,BasicKey<int>,double, RelaxedRandomizedSelector>();
    vector<int> v1 = vector<int>();
    v1.push_back(24);
    v1.push_back(33);
    vector<int> v2 = vector<int>();
    v2.push_back(23);
    v2.push_back(12);
    vector<int> v3 = vector<int>();
    v3.push_back(25);
    v3.push_back(77);
    vector<int> v4 = vector<int>();
    v4.push_back(3);
    v4.push_back(123);
    vector<int> v5 = vector<int>();
    v5.push_back(56);
    v5.push_back(20);
    vector<int> v6 = vector<int>();
    v6.push_back(87);
    v6.push_back(41);
    vector<int> v7 = vector<int>();
    v7.push_back(55);
    v7.push_back(4);
    vector<int> v8 = vector<int>();
    v8.push_back(23);
    v8.push_back(89);
    vector<int> v9 = vector<int>();
    v9.push_back(99);
    v9.push_back(1);
    vector<int> v10 = vector<int>();
    v10.push_back(120);
    v10.push_back(130);
    BasicKey<int> k1 = BasicKey<int>(v1);
    BasicKey<int> k2 = BasicKey<int>(v2);
    BasicKey<int> k3 = BasicKey<int>(v3);
    BasicKey<int> k4 = BasicKey<int>(v4);
    BasicKey<int> k5 = BasicKey<int>(v5);
    BasicKey<int> k6 = BasicKey<int>(v6);
    BasicKey<int> k7 = BasicKey<int>(v7);
    BasicKey<int> k8 = BasicKey<int>(v8);
    BasicKey<int> k9 = BasicKey<int>(v9);
    BasicKey<int> k10 = BasicKey<int>(v10);
    t.insert(k1, 1);
    t.insert(k2, 2);
    t.insert(k3, 3);
    
    t.insert(k4, 4);
    t.insert(k5, 5);
    t.insert(k6, 6);
    t.insert(k7, 7);
    t.insert(k8, 8);
    KdTree<2,BasicKey<int>,double, RelaxedRandomizedSelector>::iterator it = t.find(k7);
    if(it != t.end()){
        cout << "Trobat " << (*it).second << endl;
    }else{
        cout << "No trobat" << endl;
    }
    /*while(it != t.end()){
        cout << (*it).second << endl;
        ++it;
    }*/
    /*for(KdTree<2,BasicKey<int>,double, RelaxedRandomizedSelector>::iterator it2 = t.begin(); it2 != t.end(); ++it2) {
        (*it2).second = (*it2).second + 1;
    }*/
    t.print();

    KdTree<2,BasicKey<int>,Test, RelaxedRandomizedSelector> t2 = KdTree<2,BasicKey<int>,Test, RelaxedRandomizedSelector>();
    KdTree<2,BasicKey<int>,Test, RelaxedRandomizedSelector> t3 = KdTree<2,BasicKey<int>,Test, RelaxedRandomizedSelector>();
    Test tst = Test(20);
    t2.insert(k1, tst);
    t3.insert(k1, tst);
    KdTree<2,BasicKey<int>, Test, RelaxedRandomizedSelector>::iterator it3 = t2.find(k1);
    (*it3).second.valor = 22;
    KdTree<2,BasicKey<int>, Test, RelaxedRandomizedSelector>::iterator it4 = t3.find(k1);
    if(it3 != t2.end()){
        cout << "Trobat ";
        (*it3).second.print();
        cout << endl;
    }else{
        cout << "No trobat" << endl;
    }
    if(it4 != t3.end()){
        cout << "Trobat ";
        (*it4).second.print();
        cout << endl;
    }else{
        cout << "No trobat" << endl;
    }
    Minkowski<kdtree::Key_type, 2> f = Minkowski<kdtree::Key_type, 2>();

    cout << f(k8,k1) << endl;
    cout << f(k8,k2) << endl;
    cout << f(k8,k3) << endl;
    cout << f(k8,k4) << endl;
    cout << f(k8,k5) << endl;
    cout << f(k8,k6) << endl;
    cout << f(k8,k7) << endl;
    cout << f(k8,k8) << endl;
    vector<int> max = vector<int>();
    max.push_back(numeric_limits<int>::max());
    max.push_back(numeric_limits<int>::max());
    vector<int> min = vector<int>();
    min.push_back(numeric_limits<int>::min());
    min.push_back(numeric_limits<int>::min());
    BasicKey<int> kmin = BasicKey<int>(min);
    BasicKey<int> kmax = BasicKey<int>(max);
    kdtree::NNquery q = kdtree::NNquery(k8, kmin, kmax);
    kdtree t0 = kdtree();
    t0.insert(k1,23);
    //t.remove(k3);
    kdtree::NNSearcher<Minkowski<kdtree::Key_type, 2> > s = t.query<Minkowski<kdtree::Key_type, 2> >(q);
    kdtree::NNSearcher<Minkowski<kdtree::Key_type, 2> >::qiterator qit = s.begin();

    while(qit != s.end()) {
        cout << (*qit).second << endl;
        ++qit;
    }
    cout << "RANGE" << endl;
    kdtree::Rangequery qr = kdtree::Rangequery(kmin, kmax);
    kdtree::RangeSearcher sr = t.query(qr);
    kdtree::RangeSearcher::qiterator qrit = sr.begin();
    while(qrit != sr.end()) {
        for(int i = 0; i < (*qrit).first.size(); ++i) {
            cout << (*qrit).first[i] << " ";
        }
        cout << endl;
        ++qrit;
    }






    t.print();
    cout << t.getSize() << endl;
    cout << "--------------------------------------------------------" << endl;
    t.remove(k3);
    t.print();
    cout << t.getSize() << endl;
    return 1;
}
