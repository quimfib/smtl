#ifndef _SMTL_VPSelectors_H
#define _SMTL_VPSelectors_H

#include <algorithm>
#include <vector>

using namespace std;
/**
    @file VPSelectors.hpp
    In this file there are implemented the different types of selectors defined for the VpTree.
*/

/**
    @class MedianSelector
    This selector builds the tree by picking a pivot at random, and then picking the radius such that
    leaves half the elements inside the ball and the other half outside
*/
template<typename Metric, typename Value>
class MedianSelector{
    /**
        @class MedianComparator
        This class is used as a comparato for the std::nth, in order to find the median value
    */
    class MedianComparator {
        Value cmp;
    public:
        MedianComparator(Value orig): cmp(orig){}
        bool operator()(const Value& o1, const Value& o2){
            Metric f = Metric();
            typename Metric::image_type i1 = f(cmp, o1);
            typename Metric::image_type i2 = f(cmp, o2);
            return i1<i2;
        }
    };
    vector<Value> smaller; ///collection of elements that fall inside the ball
    vector<Value> greater; ///collection of elements that fall outside the ball
    Value pivot;
    typename Metric::image_type radius;
    public:
        MedianSelector(){
            smaller = vector<Value>();
            greater = vector<Value>();
        }
        /**
            @param v vector of elements for which we want to find a pivot and radius and split them
            This method calculates the pivot and radius and updates smaller, greater, pivot and radius accordingly
        */
        void process(vector<Value> v){
            smaller = vector<Value>();
            greater = vector<Value>();
            if(v.size() == 1){
                pivot = *(v.begin());
                radius = 0;
            }else{
                Random R;
                std::swap(*(v.begin()), *(v.begin()+R(v.size())));
                pivot = *(v.begin());
                v.erase(v.begin());
                MedianComparator cmp = MedianComparator(pivot);
                int mid = v.size()/2;
                std::nth_element(v.begin(),v.begin()+mid,v.end(),cmp);
                Metric f = Metric();
                radius = f(pivot, *(v.begin()+mid));
                smaller.insert(smaller.end(),v.begin(), v.begin()+mid);
                greater.insert(greater.end(),v.begin()+(mid), v.end());
            }
        }
        typename Metric::image_type getRadius(){
            return radius;
        }
        Value getPivot(){
            return pivot;
        }
        vector<Value> closerElements(){
            return smaller;
        }
        vector<Value> fartherElements(){
            return greater;
        }
};
#endif //_SMTL_VPSelectors_H