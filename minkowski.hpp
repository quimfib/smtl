#ifndef _SMTL_MINKOWSKI_H
#define _SMTL_MINKOWSKI_H
/**
	@file minkowski.hpp
	Class borrowed from the Spatial and Metric template library
	of Maria Merce Pons Crepso, turned in fullfilment of the "Màster en Computació"
	of the Universitat Politècnica de Catalunya (UPC).

	This class implements the minkowski norm
*/

#include <cmath>

//#define NDEBUG  //si esta definit, desactiva assert
#include <assert.h>


namespace smtl {

template <typename Numeric>
inline Numeric abs( Numeric x ) 
{
	return x >= Numeric() ? x : -x;
};

template <typename T>
inline T max( const T& x, const T& y ) 
{
	return x >= y ? x : y;
};


template <typename MetricSpace, int Dim>
class Minkowski {
public:
	typedef double image_type;
	double operator()(const MetricSpace& x, const MetricSpace& y)
	{
		double s = 0;
		for ( int i = 0; i < x.size(); i++ )
		{
			s += pow( abs(x[i]-y[i]), (double)Dim );
		}
		return pow( s, 1/(double)Dim );
	};
};


template <class MetricSpace>
class Minkowski<MetricSpace,1> {  //Manhattan
public:
	typedef double image_type;
	double operator()(const MetricSpace& x, const MetricSpace& y)
	{
		assert ( x.size() == y.size() );
		double s = 0;
		for( int i = 0; i < x.size(); i++ )
		{
			s += abs(x[i]-y[i]);
		}
		return s;
	};
};


template <class MetricSpace>
class Minkowski<MetricSpace,2> {	//Euclidian
public:
	typedef double image_type;
	double operator()(const MetricSpace& x, const MetricSpace& y)
	{
		assert ( x.size() == y.size() );
		double s = 0;
		for ( int i = 0; i < x.size(); i++ )
		{
			s += pow( x[i]-y[i], 2 );
		}
		return sqrt(s);
	};
};


template <class MetricSpace>
class Minkowski<MetricSpace,-1> {	// Dim = -1 => L_infinity - norm --> Chebyshev
public:
	typedef double image_type;
	double operator()(const MetricSpace& x, const MetricSpace& y)
	{
		assert ( x.size() == y.size() );
		double s = 0;
		for (int i = 0; i < x.size(); i++)
		{
			s = max(abs(x[i]-y[i]), s);
		}
		return s;
	};
};


}	// end namespace stml
#endif	//_STML_MINKOWSKI_H

