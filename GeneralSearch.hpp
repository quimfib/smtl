


template<typename Search, template<typename> class Container>
class GeneralSearch{
private:
    typedef typename Search::Element Element;
    Element current;
    Container<Element>* c;
    Search* sp;

    class Iterator{
    private:
        GeneralSearch* s;
        bool isEnd;
    public:
        Iterator(GeneralSearch* _s, bool _end){
            s = _s;
            isEnd = _end;
        }
        Element operator*(){
            return s->retrieveTop();
        }
        Iterator& operator++(){
            if(not isEnd) {
                s->step();
                isEnd = s->finished();
            }
        }
        bool operator==(Iterator& it){
            if(isEnd && it.isEnd) return true;
            else return s->retrieveTop() == it.s->retrieveTop();
        }
        bool operator!=(Iterator& it){
            return not(*this == it);
        }
    };
    void step(){
        while(c.top().notResult() and not c.empty()){
            vector<Element> v = sp->processElement(c->top());
            c->pop();
            for(typename vector<Element>::iterator it = v.begin(); it != v.end(); ++it) c->push(*it);
        }
    }
    void init(Element first){
        vector<Element> v = sp->processElement(first);
        for(typename vector<Element>::iterator it = v.begin(); it != v.end(); ++it) c->push(*it);
    }
    friend class Iterator;
public:
    GeneralSearch(Element first, Search* _sp){
        c = new Container<Element>();
        sp = _sp;
        init(first); 
    }
    ~GeneralSearch(){
        delete c;
        delete sp;
    }
    Element retrieveTop(){
        return c->top();
    }
    bool finished(){
        return c->empty();
    }
    Iterator begin(){
        return Iterator(this, false);
    }
    Iterator end(){
        return Iterator(this, true);
    }
};