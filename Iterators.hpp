#ifndef _SMTL_ITERATORS_H
#define _SMTL_ITERATORS_H

#include <stack>

using namespace std;
/**
    @file Iterators.hpp
    This file contains the definitions of most iterators used in the SMTL. We take advantadge of the fact
    that all structures currently in the SMTL follow the same hierarchy
*/

/**
    @class Iterator
    General mutable iterator that implements a inorder traversal of a hierarchical structure, where each node has 
    left and right children.
*/
template<typename Node>
class Iterator : public std::iterator<std::forward_iterator_tag, typename Node::Element> {
        private:
            Node* current;
            stack<Node*> path;
        void begin(){
            while(current != 0){
                path.push(current);
                current = current->getLeft();
            }
            if(not path.empty()) {
                current = path.top();
                path.pop();
                if(current->getRight() != 0){
                    path.push(current->getRight());
                    Node* aux = current->getRight();
                    aux = aux->getLeft();
                    while(aux != 0){
                        path.push(aux);
                        aux = aux->getLeft();
                    }
                }
            }
        }
        public:
            /**
                @root pointer to the root of the hierarchy
                @isBegin boolean indicating whether this iterator should point to the beginning or
                to null (end)
            */
            Iterator(Node* root, bool isBegin){
                if(isBegin) {
                    current = root;
                    begin();
                }else{
                    current = 0;
                }
            }
            ~Iterator(){}
        
        typename Node::value& operator*(){
            return current->getElement();
        }
        typename Node::value& operator*() const {
            return current->getElement();
        }
        bool operator==(const Iterator& vi) const{
            return current == vi.current;
        }
        bool operator!=(const Iterator& vi) const{
            return not(*this == vi);
        }
        /**
            Performs an inorder traversal of the structure. When it gets to end sets the iterator pointer
            to null
        */
        Iterator& operator++(){
            if(not path.empty()){
                current = path.top();
                path.pop();
                if(current->getRight() != 0){
                    path.push(current->getRight());
                    Node* aux = current->getRight();
                    aux = aux->getLeft();
                    while(aux != 0){
                        path.push(aux);
                        aux = aux->getLeft();
                    }
                }
            }else current = 0;
        }
    };

/**
    @class IteratorFind
    Iterator for data structures that support find. This iterator is templatized by node type and key type (which is
    used for he find method)
*/
template<typename Node, typename Key>
class IteratorFind : public std::iterator<std::forward_iterator_tag, typename Node::Element> {
        private:
            Node* current;
            stack<Node*> path;
        /**
            @param k key we're looking for
        */
        void find(const Key& k){    
            bool found = false;
            int i = 0;
            while(current != 0 and not found) {
                i = current->getDiscriminant();
                if(k[i] < current->getKey()[i]) {
                    path.push(current);
                    current = current->getLeft();
                }else if (k[i] > current->getKey()[i]) {
                    current = current->getRight();
                }else{
                    found = k == current->getKey();
                    if(not found) current = current->getRight();
                }
            }  
        }
        void begin(){
            while(current != 0){
                path.push(current);
                current = current->getLeft();
            }
            if(not path.empty()) {
                current = path.top();
                path.pop();
                if(current->getRight() != 0){
                    path.push(current->getRight());
                    Node* aux = current->getRight();
                    aux = aux->getLeft();
                    while(aux != 0){
                        path.push(aux);
                        aux = aux->getLeft();
                    }
                }
            }
        }
        public:
            /**
                @param _k key sought, when performing the search
                @param root pointer to the root of the hierarchy
                This constructor is used when we want to initialize the iterator as a result of a search
            */
            IteratorFind(const Key& _k, Node* root) {
                current = root;
                find(_k);
            }
            /**
                @root pointer to the root of the hierarchy
                @isBegin boolean indicating whether this iterator should point to the beginning or
                to null (end)
            */
            IteratorFind(Node* root, bool isBegin){
                if(isBegin) {
                    current = root;
                    begin();
                }else{
                    current = 0;
                }
            }
            ~IteratorFind(){}
        
        typename Node::Element& operator*(){
            return current->getElement();
        }
        bool operator==(const IteratorFind& ki) const{
            return current == ki.current;
        }
        bool operator!=(const IteratorFind& ki) const{
            return not(*this == ki);
        }
        IteratorFind& operator++(){
            if(not path.empty()){
                current = path.top();
                path.pop();
                if(current->getRight() != 0){
                    path.push(current->getRight());
                    Node* aux = current->getRight();
                    aux = aux->getLeft();
                    while(aux != 0){
                        path.push(aux);
                        aux = aux->getLeft();
                    }
                }
            }else current = 0;
        }
    };
/**
    @file QueryIterator
    QueryIterator is an iterator prepared to be used in conjunction with the searcher objects
*/
/**
    @class QueryIterator
    It is parametrized by a searcher object. It expects the sercher object to have the following methods defined:
    -getLast() (and the return type of getLast() must have a getPayLoad() method)
    -processNext() : calculates the next object in the search
*/
template<typename SearchObject>
class QueryIterator : public std::iterator<std::forward_iterator_tag, typename SearchObject::Element::value>{
  bool isEnd; /// flag indicating whether the iterator points to end
  SearchObject* s; ///pointer for the Search object
  //typename SearchObject::Element last;
  public:
    /**
     @param _s The search object that will calculate the elements of the query
     @param _end : A boolean indicating whether this iterator should point to end from the start
     */
    QueryIterator(SearchObject* _s, bool _end): s(_s), isEnd(_end){}
    ~QueryIterator(){}
    /**
        Retrieves the last calculated item from the Search object
    */
    typename SearchObject::Element::value operator*(){
        return s->getLast().getPayload();
    }
    /**
        Retrieves the next item in the search. If it doesn't exist points to end
    */
    QueryIterator operator++(){
        if(not isEnd) {
            bool hasNext = s->processNext();
            isEnd = not hasNext;
        }
    }
    /**
        Equality test. Is used by comparing whether both are end and if needed to which elments the getLast() is pointing
    */
    bool operator==(QueryIterator it){
        if(isEnd and it.isEnd) return true;
        else if(isEnd or it.isEnd) return false;
        else return s->getLast().n == it.s->getLast().n;
    }
    /**
        Difference operator. defined as the negation of the equality operator
    */
    bool operator!=(QueryIterator it){
        return not(*this == it);
    }
};
#endif //_SMTL_ITERATORS_H