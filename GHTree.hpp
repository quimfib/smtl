#ifndef _SMTL_GHTREE_H
#define _SMTL_GHTREE_H

#include <stack>
#include <iomanip>
#include "Iterators.hpp"
#include "Queries.hpp"

using namespace std;
/**
    @file GHTree.hpp
    In this File there is the definition of the GHTree class and all the necessary classes to perform queries
    on this data structure
*/

/**
    @class GHTree
    @param Value the type of the elements that the tree will hold
    @param Metric the metric function that will define the distance between two objects
*/

template<typename Value, typename Metric>
class GHTree{
    /**
        @class GHNode
        The type of the GHtree nodes.
    */
    class GHNode {
        friend class GHTree;
        Value v; ///The element we want to store
        GHNode* child; ///Pointer to the child node
        GHNode* sibling; ///Pointer to the sibling node
        int size; ///Size of the tree rooted at this node
    public:
        typedef Value value;
        GHNode(): child(0), sibling(0), size(0){}
        GHNode(const Value& _v): v(_v), child(0), sibling(0), size(1){}
        ~GHNode(){
            delete child;
            delete sibling;
        }
        /**
            Since the "theoretical" nodes of the GH tree are split into two GHNode connected through the
            sibling pointer, it makes sense to ask if a given value is child of a given node, whic is the same
            as asking if it is closer to this node than to its sibling.
        */
        bool isChild(const Value& _v){
            Metric f = Metric();
            if(f(v, _v) <= f(sibling->v, v)) return true;
            else return false;
        }
        /**
            Will return false if sibling is null, true otherwise.
        */
        bool initialized(){
            return sibling != 0;
        }
        Value& getElement(){
            return v;
        }
        GHNode* getChild(){
            return child;
        }
        GHNode* getSibling(){
            return sibling;
        }
        GHNode* getLeft(){
            return child;
        }
        GHNode* getRight(){
            if(sibling != 0) return sibling->child;
            else return 0;
        }
        void print(){
            cout << v;
        }
    };
    /**
        @class GHIterator
        Since the structure of the GH tree is a bit different from the other trees, we opted to define a
        specific for this data structure. In the future, we could look to integrate it into the iterators class
    */
    class GHIterator : public std::iterator<std::forward_iterator_tag, typename GHNode::value>{
        private:
            GHNode* current;
            stack<GHNode*> path;
        void begin(){
            while(current != 0){
                path.push(current);
                current = current->child;
            }
            ++(*this);
        }
        public:
        GHIterator(GHNode* root, bool isBegin){
            if(isBegin) {
                current = root;
                begin();
            }else{
                current = 0;
            }
        }
        ~GHIterator(){}
        
        Value& operator*(){
            return current->v;
        }
        Value& operator*() const {
            return current->v;
        }
        bool operator==(const GHIterator& vi) const{
            return current == vi.current;
        }
        bool operator!=(const GHIterator& vi) const{
            return not(*this == vi);
        }
        GHIterator& operator++(){
            if(not path.empty()) {
                current = path.top();
                path.pop();
                if(current->sibling != 0){
                    GHNode* aux = current->sibling->child;
                    while(aux != 0){
                        path.push(aux);
                        aux = aux->child;
                    }
                    path.push(current->sibling);
                }
            }else current = 0;
        }
    };
//-------------------------------- Start definition GH-tree----------------------------------------------------

    GHNode* root;

    /**
        @param n node for which we want to know the size of the tree rooted at it
        Function to retrieve the size of a tree. This is just a wrapper to deal with the case of the empty
        tre (null pointer)
    */
    int nodeSize(GHNode* n){
        if(n == 0) return 0;
        else return n->size;
    }
    /**
        @param v Value to be inserted
        @param current Pointer to the GHNode in which we want to insert v
    */
    GHNode* insert(const Value& v, GHNode* current){
        if(current == 0) {
            return new GHNode(v);
        }else{
            Metric f = Metric();
            if(f(current->v, v) <= 0) return current;
            if(current->sibling != 0) {
                if(f(current->getSibling()->v, v) <= 0) return current;
                if(current->isChild(v)) current->child = insert(v, current->child);
                else current->sibling->child = insert(v, current->sibling->child);
            }else{
                current->sibling = new GHNode(v);
            }
        }
        current->size = 1 + nodeSize(current->child) + nodeSize(current->sibling);
        return current;
    }
public:
    typedef GHIterator iterator;
    typedef NNQuery<Value> NNquery;
    GHTree(){
        root = 0;
    }
    ~GHTree(){
        delete root;
    }
    iterator begin(){
        return iterator(this->root, true);
    }
    iterator end(){
        return iterator(this->root, false);
    }
    void insert(const Value& v){
        root = insert(v,root);
    }
    void print(){
        print(this->root);
    }
    /**
        Simple printing function, for visualization purposes
    */
    void print(GHNode* p, string tag = "root", int indent=0)
    {
        if(p != 0) {
            if (indent) {
                std::cout << std::setw(indent) << ' ' << tag;
            }
            p->print();
            if(p->sibling != 0) {
                cout << " | ";
                p->sibling->print();
            }
            cout << endl;
            if(p->child) {
                print(p->child, "Close First", indent+4);
            }
            if(p->sibling){
                print(p->sibling->child, "Close Second", indent+4);
            }
        }
    }
    int getSize(){
        return this->root->size;
    }

        //----------------------------- Nearest Neighbour query ---------------------------------------
    /**
        @class NNSearcher
        This class is responsible for implementing the Hierarchical Ranking Search (HRS) for the nearest neighbour query
    */
    class NNSearcher{
        /**
            @class SearchElement
            The elements that will be pushed into the priority queue. It works as a wrapper for a pointer to a GHtree
            node and its score (distance in this case)
        */
        class SearchElement{
        public:
            typedef Value value;
            typename GHTree::GHNode* n; ///Pointer to the node
            typename Metric::image_type score; ///Score, in this case it's a distance
            bool isResult; ///Whether the element is of type 0 or 1
            SearchElement(){}
            //SearchElement(const Node* _n, bool _result, typename Metric::image_type _score, Key min=n->getKey(), Key max=n->getKey()):n(_n), isResult(_result),score(_score),box(min,max){}
            SearchElement(typename GHTree::GHNode* _n, bool _result, typename Metric::image_type _score): n(_n), isResult(_result),score(_score){}
            /**
                Comparison operator. Elements of type 0 have priority over type 1.
            */
            bool operator<(const SearchElement& e) const{
                if(isResult and e.isResult) return score > e.score;
                else return isResult;
            }
            bool notResult() const{
                return not isResult;
            }
            /**
                Method that retrieves the proper element from a pointer to a GHNode
            */
            Value getPayload(){
                return n->getElement();
            }
            /*SearchElement& operator=(SearchElement& el){
                n = el.n;
                box = el.box;
                score = el.score;
                isResult = el.isResult;
            }*/
        };
    public:
        Metric f;
        typedef SearchElement Element;
        typedef NNQuery<Value> Query;
        typedef QueryIterator<NNSearcher> qiterator;
        Query q;
        priority_queue<Element> pq;
        NNSearcher(){}
        NNSearcher(Query _q): q(_q), f(Metric()), pq(priority_queue<Element>()){}
        /**
            @param n A pointer to a node of the tree

            This method is used to initiate the search. It processes the root of the tree and pushes the required
            elements into the priority queue
        */
        void initFirst(typename GHTree::GHNode* n){
            if(n != 0) {
                Element first = Element(n, false, 0);
                pq.push(first);
                while(not pq.empty() and pq.top().notResult()){
                    Element x = pq.top();
                    pq.pop();
                    vector<Element> v = processElement(x);
                    for(typename vector<Element>::iterator it = v.begin(); it != v.end(); ++it) pq.push(*it);
                }
            }
        }
        /**
            @param x A Search element.

            Given a search element, generates the required elements, with the appropriate distances computed
        */
        vector<Element> processElement(Element& x){
            vector<Element> v = vector<Element>();
            typename Metric::image_type d1 = f(q.v, x.n->getElement());
            Element p1 = Element(x.n, true, d1);
            v.push_back(p1);

            bool hasSibling = x.n->getSibling() != 0;
            if(hasSibling){
                Element p2 = Element(x.n->getSibling(), true, f(x.n->getSibling()->getElement(), q.v));
                v.push_back(p2);
                typename Metric::image_type d2 = f(q.v, x.n->getSibling()->getElement());
                if(x.n->getRight() != 0) {
                    typename Metric::image_type score = std::max((d2-d1)/2, (typename Metric::image_type)0);
                    Element leftElem = Element(x.n->getRight(), false, score);
                    v.push_back(leftElem);
                }
                if(x.n->getLeft() != 0) {
                    typename Metric::image_type score = std::max((d1-d2)/2, (typename Metric::image_type)0);
                    Element rightElem = Element(x.n->getLeft(), false, score);
                    v.push_back(rightElem);
                }
            }
            return v;
        }
        /**
            Interface method used by the QueryIterator. On invocation calculates the next type 0 element of the search
        */
        bool processNext(){
            if(not pq.empty()) pq.pop();
            while(not pq.empty() and pq.top().notResult()){
                Element x = pq.top();
                pq.pop();
                vector<Element> v = processElement(x);
                for(typename vector<Element>::iterator it = v.begin(); it != v.end(); ++it) pq.push(*it);
            }
            return not pq.empty();
        }
        /**
            Retrieve the last type 0 element
        */
        Element getLast(){
            return pq.top();
        }

        qiterator begin(){
            return qiterator(this, pq.empty());
        }

        qiterator end(){
            return qiterator(this, true);
        }
    };
    /**
        Specialized call to query for the Nearest neighbor search
    */
    NNSearcher query(NNQuery<Value> q){
        NNSearcher s = NNSearcher(q);
        s.initFirst(this->root);
        return s;
    }
};
#endif //_SMTL_GHTREE_H