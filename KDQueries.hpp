#include "HyperRectangle.hpp"

class NNQuery{
  Key k;
  NNQuery(const Key& _k): k(_k){}  
};
template<typename Metric>
class NNSearcher{
    typedef HyperRectangle<Key, Dim> BBox;
    Metric f;
    class SearchElement{
    public:
        KdNode* n;
        HyperRectangle box;
        typename Metric::image_type score;
        bool isConcrete;
        SearchElement(KdNode* _n, bool _concrete, typename Metric::image_type _score, Key min=n->getKey(), Key max=n->getKey()):
                        n(_n), isConcrete(_concrete),score(_score),box(min,max){}
        SearchElement(KdNode* _n, bool _concrete, typename Metric::image_type _score, HyperRectangle _box):
                        n(_n), isConcrete(_concrete),score(_score),box(_box){}
        bool operator<(const SearchElement& e) const {
            if(isConcrete and e.isConcrete) return score < e.score;
            else return isConcrete;
        }
    };
    typedef SearchElement Element;
    NNQuery q;
    NNSearcher(NNQuery _q): q(_q), f(Metric()){}
    vector<Element> processElement(Element& x){
        vector<Element> v = vector<Element>();
        Element root = Element(x.n, true, f(x.n->getKey(), q.k));
        HyperRectangle box = root.box;
        v.push_back(root);
        int d = x->getDiscriminant();
        if(x.n->getLeft() != 0){
            HyperRectangle leftBox = box;
            leftBox.upper[d] = x->n.getKey()[d];
            typename Metric::image_type dist = leftBox.distToPoint(q.k);
            Element leftElem = Element(x.n->getLeft(), false, dist, leftBox);
            v.push_back(leftElem);  
        }
        if(x.n->getRight() != 0){
            HyperRectangle rightBox = box;
            rightBox.lower[d] = x->n.getKey()[d];
            typename Metric::image_type dist = rightBox.distToPoint(q.k);
            Element rightElem = Element(x.n->getLeft(), false, dist, rightBox);
            v.push_back(rightElem); 
        }
        return v;
    }
};