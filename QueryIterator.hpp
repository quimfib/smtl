#ifndef _SMTL_QUERYITERATOR_H
#define _SMTL_QUERYITERATOR_H

template<typename SearchObject>
class QueryIterator{
  bool isEnd;
  SearchObject* s;
  //typename SearchObject::Element last;
  public:
    QueryIterator(SearchObject* _s, bool _end): s(_s), isEnd(_end){}
    ~QueryIterator(){}

    typename SearchObject::Element::Payload operator*(){
        return s->getLast().getPayload();
    }
    QueryIterator operator++(){
        if(not isEnd) {
            bool hasNext = s->processNext();
            isEnd = not hasNext;
        }
    }
    bool operator==(QueryIterator it){
        if(isEnd and it.isEnd) return true;
        else if(isEnd or it.isEnd) return false;
        else return s->getLast().n == it.s->getLast().n;
    }
    bool operator!=(QueryIterator it){
        return not(*this == it);
    }
};
#endif //_SMTL_QUERYITERATOR_H