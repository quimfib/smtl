#ifndef _SMTL_HYPERRECTANGLE_H
#define _SMTL_HYPERRECTANGLE_H
#include "minkowski.hpp"

/**
    @file HyperRectangle.hpp
    This file defines the needed classes to have hyperrectangles    
*/

/**
    @class HyperRectangle
    Defines a hyperrectangle. It is parametrized by the Point (vertices) 
    of the hyperrectangle and the Dimension of the Points(vertices)
*/
template<typename Point, int Dim>
class HyperRectangle{
public:
    Point upper; ///Upper vertex of the hyperrectangle
    Point lower; ///Lower vertex of the hyperrectangle
    int dimension; ///dimension of the hyperrectangle
    
    HyperRectangle(){}
    /**
        @param _lower lower vertex of the hyperrectangle
        @param _upper upper vertex of the hyperrectangle
    */
    HyperRectangle(Point _lower,Point _upper){
        lower = _lower;
        upper = _upper;
        dimension = Dim;
    }
    ~HyperRectangle(){}
    void setUpper(Point _upper){
        upper = _upper;
    }
    void setLower(Point _lower){
        lower = _lower;
    }
    /**
        @param p the point to which we want to calculate the minimum distance
        @param f a distance function
    */
    template<typename Metric>
    typename Metric::image_type distToPoint(Point p, Metric f){
    //double distToPoint(Point p){
        //smtl::Minkowski<Point, 2> f = Metric();
        Point v = p;
        bool bounded = true;
        for(int i = 0; i < Dim; ++i){
            if(lower[i] <= p[i] and p[i] <= upper[i]) v[i] = p[i];
            else {
                bounded = false;
                if(p[i] < lower[i]) p[i] = lower[i];
                else p[i] = upper[i];
            }
        }
        //This If is to account for the fact that due to precision errors we are not guaranteed
        //0 distance if the query point is bounded by the hyperrectangle
        if(not bounded) return f(p,v);
        else return 0;
    }
};
#endif //_SMTL_HYPERRECTANGLE_H