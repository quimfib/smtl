#ifndef _SMTL_SELECTORS_H
#define _SMTL_SELECTORS_H

#include "random.cpp"

/**
    @file Selectors.hpp
    In this file there are implemented the different types of selectors defined for the Kdtree.
*/
using namespace std;

enum NextOp {INSERT, LEFT, RIGHT, DUPLICATE};

/** 
    @class StdSelector
    Standard selector, cyclic coordinate picking
*/
template<typename Key, int Dim>
class StdSelector{
    int lastDiscriminant;
public:
    StdSelector(){
        lastDiscriminant = -1;
    }
    ~StdSelector(){}
    /**
        @param key key that is being inserted
        @param nodKey key of the current node being explored
        @param size size of the tree rooted at the node being explored
        @param i discriminant of the current node being explored

        This method returns the next action that the insertion method of the kdtree should do
        from the set {insert, left, right, duplicate}
    */
    NextOp next(const Key& key, const Key& nodeKey, int size, int i){
        lastDiscriminant = i;
        if(key[i] < nodeKey[i]){
            return LEFT;
        }else if(key[i] > nodeKey[i]) {
            return RIGHT;
        }else{
            if(key == nodeKey) return DUPLICATE;
            else return RIGHT;
        }
    }
    int calculateDiscriminant(){
        return (lastDiscriminant+1)%Dim;
    }
    /**
        If true, when deleting will rebuild the tree rooted at the deleted node using the split and join methods.
        If false, will reinsert the children.
    */
    bool deleteRoot(){
        return false;
    }
};
/**
    @class SquarishSelector
    Picks the discriminant by splitting the longest edge, in case of multiple candidates, picks one at random
*/
template<typename Key, int Dim>
class SquarishSelector{
    Key upper;
    Key lower;
    vector<std::pair<bool,bool> > init;
public:
    SquarishSelector(){
        upper = Key(Dim);
        lower = Key(Dim);
        init = vector<std::pair<bool,bool> >(Dim, make_pair(false,false));
    }
    ~SquarishSelector(){}
    /**
        @param key key that is being inserted
        @param nodKey key of the current node being explored
        @param size size of the tree rooted at the node being explored
        @param i discriminant of the current node being explored

        This method returns the next action that the insertion method of the kdtree should do
        from the set {insert, left, right, duplicate}
    */
    NextOp next(const Key& key, const Key& nodeKey, int size, int i){
        if(not init[i].first) {
            init[i].first = true;
            lower[i] = nodeKey[i];
        }
        if(not init[i].second) {
            init[i].second = true;
            upper[i] = nodeKey[i];
        }
        if(init[i].first and init[i].second){
            upper[i] = std::max(upper[i], nodeKey[i]);
            lower[i] = std::min(lower[i], nodeKey[i]);
        }
        if(key[i] < nodeKey[i]){
            return LEFT;
        }else if(key[i] > nodeKey[i]) {
            return RIGHT;
        }else{
            if(key == nodeKey) return DUPLICATE;
            else return RIGHT;
        }
    }
    int calculateDiscriminant(){
        vector<typename Key::key_comp> results = vector<typename Key::key_comp>();
        typename Key::key_comp bestDiff = (typename Key::key_comp) 0;
        for(int i = 0; i < Dim; ++i){
            if(init[i].first and init[i].second){
                typename Key::key_comp d = upper[i]-lower[i];
                if(d > bestDiff){
                    results.clear();
                    results.push_back(d);
                }else if(d == bestDiff) results.push_back(d);
            }
        }
        Random R;
        return results[R(results.size())];
    }
    /**
        If true, when deleting will rebuild the tree rooted at the deleted node using the split and join methods.
        If false, will reinsert the children.
    */
    bool deleteRoot(){
        return false;
    }
};
/**
    @class RelaxedSelector
    Basically like the Standard selector, only the discriminant is picked at random. 
*/
template<typename Key, int Dim>
class RelaxedSelector{
public:
    RelaxedSelector(){}
    ~RelaxedSelector(){}
    /**
        @param key key that is being inserted
        @param nodKey key of the current node being explored
        @param size size of the tree rooted at the node being explored
        @param i discriminant of the current node being explored

        This method returns the next action that the insertion method of the kdtree should do
        from the set {insert, left, right, duplicate}
    */
    NextOp next(const Key& key, const Key& nodeKey, int size, int i){
        if(key[i] < nodeKey[i]){
            return LEFT;
        }else if(key[i] > nodeKey[i]) {
            return RIGHT;
        }else{
            if(key == nodeKey) return DUPLICATE;
            else return RIGHT;
        }
    }
    int calculateDiscriminant(){
        Random R;
        return R(Dim);
    }
    /**
        If true, when deleting will rebuild the tree rooted at the deleted node using the split and join methods.
        If false, will reinsert the children.
    */
    bool deleteRoot(){
        return false;
    }
};
/**
    @class RelaxedRandomizedSelector
    Picks the discriminant at random, and also deletes at root, meaning that when we delete a node we rebuild the tree 
    rooted at that node by using split and join
*/
template<typename Key, int Dim>
class RelaxedRandomizedSelector{
    Random r;
public:
    RelaxedRandomizedSelector(): r(Random()){}
    ~RelaxedRandomizedSelector(){}
    /**
        @param key key that is being inserted
        @param nodKey key of the current node being explored
        @param size size of the tree rooted at the node being explored
        @param i discriminant of the current node being explored

        This method returns the next action that the insertion method of the kdtree should do
        from the set {insert, left, right, duplicate}
    */
    NextOp next(const Key& key, const Key& nodeKey, int size, int i){
        if(r(size) == 0) return INSERT;
        if(key[i] < nodeKey[i]){
            return LEFT;
        }else if(key[i] > nodeKey[i]) {
            return RIGHT;
        }else{
            if(key == nodeKey) return DUPLICATE;
            else return RIGHT;
        }
    }
    int calculateDiscriminant(){
        return r(Dim);
    }
    /**
        If true, when deleting will rebuild the tree rooted at the deleted node using the split and join methods.
        If false, will reinsert the children.
    */
    bool deleteRoot(){
        return true;
    }
};
#endif //_SMTL_SELECTORS_H