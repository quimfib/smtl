#include <string>
#include <iostream>
#include <utility>
#include <vector>
#include "VpTree.hpp"
#include <cmath>
#include <typeinfo>
#include "Queries.hpp"
//#include "QueryIterator.hpp"
#include "minkowski.hpp"

using namespace std;

class Point{
public:
    double x;
    double y;
    double z;
    Point(double _x, double _y, double _z): x(_x), y(_y), z(_z){}
    Point(){}
};
   ostream& operator<<(ostream& os, const Point& p){
        os << "(" << p.x << "," << p.y << "," << p.z << ")";
        return os;
    }
class Distance{
    public:
    typedef double image_type;
    double operator()(const Point& x, const Point& y){
        double c1 = x.x - y.x;
        double c2 = x.y - y.y;
        double c3 = x.z - y.z;
        return sqrt(c1*c1 + c2*c2 + c3*c3);
    }
};

int main(){
    std::vector<Point> elems = std::vector<Point>();
    elems.push_back(Point(1,0.23,0.44));
    elems.push_back(Point(0.66,0.89,0.42));
    elems.push_back(Point(0.99,0.96,0.83));
    elems.push_back(Point(0.28,0.01,0.58));
    elems.push_back(Point(0.09,0.46,0.35));
    elems.push_back(Point(0.78,0.11,0.30));
    elems.push_back(Point(0.28,0.98,0.74));
    elems.push_back(Point(0.1,1,0.43));

    /*elems.push_back(Point(1,1,1));
    elems.push_back(Point(0,1,0));
    elems.push_back(Point(1,0,1));*/
    typedef VpTree<Point, Distance, MedianSelector> VpTree;

    VpTree tree = VpTree(elems);
    cout << "----------------------------------" << endl;
    tree.print();

    Distance f = Distance();
    //for(std::vector<Point>::iterator it = elems.begin(); it != elems.end(); ++it) cout << f(Point(0.28,0.01,0.58), *it) << " " << *it << endl;
    VpTree::NNquery q = VpTree::NNquery(Point(0.28,0.01,0.58));
    //VpTree tree2 = VpTree(std::vector<Point>());
    VpTree::NNSearcher s = tree.query(q);
    VpTree::NNSearcher::qiterator qit = s.begin();

    while(qit != s.end()) {
        cout << (*qit) << endl;
        ++qit;
    }
    cout << "HOLA" << endl;
    for(VpTree::iterator it = tree.begin(); it != tree.end(); ++it) {
        cout << *it << endl;
        *it = Point(0,0,0);
        cout << *it << endl;
    }
    return 1;
}
